import math
import torch
from torch import nn
from timm.models.vision_transformer import PatchEmbed
from timm.models.layers import trunc_normal_
from torch.utils.checkpoint import checkpoint

# adapted OFFICIAL implementation ( https://github.com/lucidrains/linformer )

# NOTE - difference official_linformer.py <-> official_linformer_not_separated.py:
# official_linformer.py: special tokens (class, distillation) are projected using E, F tensors together with all other tokens
# official_linformer_not_separated.py: special tokens (class, distillation) are NOT projected using E, F tensors together with all other tokens - they are separated before the projection and concatenated after the projection unchanged

# helper functions

def default(val, default_val):
    return val if val is not None else default_val

def get_EF(dims,method="learnable"):
    """
    Retuns the E or F matrix, initialized via xavier initialization.
    This is the recommended way to do it according to the authors of the paper.
    Includes a method for no additional params.
    """
    if method == "no_params":
        mat = torch.zeros(dims).to(device="cuda")
        torch.nn.init.normal_(mat, mean=0.0, std=1 / dims[1])
        return mat

    learnable_param = nn.Parameter(torch.zeros(dims).to(device="cuda"))
    torch.nn.init.kaiming_uniform_(learnable_param, a=math.sqrt(5))

    return learnable_param

# helper classes

# for routing arguments into the functions of the reversible layer
def route_args(router, args, depth):
    routed_args = [(dict(), dict()) for _ in range(depth)]
    matched_keys = [key for key in args.keys() if key in router]

    for key in matched_keys:
        val = args[key]
        for depth, ((f_args, g_args), routes) in enumerate(zip(routed_args, router[key])):
            new_f_args, new_g_args = map(lambda route: ({key: val} if route else {}), routes)
            routed_args[depth] = ({**f_args, **new_f_args}, {**g_args, **new_g_args})
    return routed_args

def layer_drop(layers, drop_path_list=None):
    to_drop = torch.lt(torch.empty(len(layers)).uniform_(0, 1), drop_path_list)
    blocks = [block for block, drop in zip(layers, to_drop) if not drop]
    blocks = layers[:1] if len(blocks) == 0 else blocks
    return blocks

class SequentialSequence(nn.Module):
    def __init__(self, layers, args_route={}, layer_dropout=None):
        super().__init__()
        assert all(len(route) == len(layers) for route in args_route.values()), 'each argument route map must have the same depth as the number of sequential layers'
        self.layers = layers
        self.args_route = args_route

        if layer_dropout is None:
            layer_dropout = [0. for _ in range(len(layers))]
        layer_dropout = torch.tensor(layer_dropout)

        self.layer_dropout = layer_dropout

    def forward(self, x, **kwargs):
        args = route_args(self.args_route, kwargs, len(self.layers))
        layers_and_args = list(zip(self.layers, args))

        if self.training and self.layer_dropout is not None:
            layers_and_args = layer_drop(layers_and_args, self.layer_dropout)

        for (f, g), (f_args, g_args) in layers_and_args:
            x = x + f(x, **f_args)
            x = x + g(x, **g_args)
        return x

class Residual(nn.Module):
    def __init__(self, fn):
        super().__init__()
        self.fn = fn
    def forward(self, x):
        return x + self.fn(x)

class PreNorm(nn.Module):
    def __init__(self, dim, fn, norm_layer=nn.LayerNorm):
        super().__init__()
        self.fn = fn
        self.norm = norm_layer(dim)
    def forward(self, x):
        x = self.norm(x)
        return self.fn(x)

class GELU_(nn.Module):
    def forward(self, x):
        return 0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))

GELU = nn.GELU if hasattr(nn, 'GELU') else GELU_

class FeedForward(nn.Module):
    def __init__(self, dim, mult = 4., dropout = 0., activation = None, glu = False):
        super().__init__()
        activation = default(activation, GELU)

        self.glu = glu
        self.w1 = nn.Linear(dim, int(dim * mult * (2 if glu else 1)))
        self.act = activation()
        self.dropout = nn.Dropout(dropout)
        self.w2 = nn.Linear(int(dim * mult), dim)

    def forward(self, x, **kwargs):
        if not self.glu:
            x = self.w1(x)
            x = self.act(x)
        else:
            x, v = self.w1(x).chunk(2, dim=-1)
            x = self.act(x) * v

        x = self.dropout(x)
        x = self.w2(x)
        x = self.dropout(x)
        return x

class LinformerSelfAttention(nn.Module):
    def __init__(self, method, dim, seq_len, k = 256, heads = 8, dim_head = None, one_kv_head = False, share_kv = False, attn_drop=0., proj_drop=0., E_proj_layerwise = None, qkv_bias = False, qk_scale=None):
        super().__init__()
        assert (dim % heads) == 0, 'dimension must be divisible by the number of heads'

        self.seq_len = seq_len
        self.k = k

        self.scale = qk_scale or dim_head ** -0.5

        self.heads = heads

        dim_head = default(dim_head, dim // heads)
        self.dim_head = dim_head

        self.to_q = nn.Linear(dim, dim_head * heads, bias = qkv_bias)

        kv_dim = dim_head if one_kv_head else (dim_head * heads)
        self.to_k = nn.Linear(dim, kv_dim, bias = qkv_bias)

        self.share_kv = share_kv

        self.proj_k = get_EF((seq_len, k), method) if E_proj_layerwise is None else E_proj_layerwise
        if not share_kv:
            self.to_v = nn.Linear(dim, kv_dim, bias = qkv_bias)
            self.proj_v = get_EF((seq_len, k), method)

        self.attn_drop = nn.Dropout(attn_drop)
        self.to_out = nn.Linear(dim_head * heads, dim)
        self.proj_drop = nn.Dropout(proj_drop)

    def forward(self, x, **kwargs):
        b, n, d, d_h, h, k = *x.shape, self.dim_head, self.heads, self.k

        kv_len = n
        assert kv_len == self.seq_len, f'the sequence length of the key / values must be {self.seq_len} - {kv_len} given'

        queries = self.to_q(x)

        proj_seq_len = lambda args: torch.einsum('bnd,nk->bkd', *args)

        kv_input = x

        keys = self.to_k(kv_input)
        values = self.to_v(kv_input) if not self.share_kv else keys

        kv_projs = (self.proj_k, self.proj_v if not self.share_kv else self.proj_k)

        # project keys and values along the sequence length dimension to k
        keys, values = map(proj_seq_len, zip((keys, values), kv_projs))

        # merge head into batch for queries and key / values
        queries = queries.reshape(b, n, h, -1).transpose(1, 2)

        merge_key_values = lambda t: t.reshape(b, k, -1, d_h).transpose(1, 2).expand(-1, h, -1, -1)
        keys, values = map(merge_key_values, (keys, values))

        # attention
        dots = torch.einsum('bhnd,bhkd->bhnk', queries, keys) * self.scale
        attn = dots.softmax(dim=-1)
        attn = self.attn_drop(attn)
        out = torch.einsum('bhnk,bhkd->bhnd', attn, values)

        # split heads
        out = out.transpose(1, 2).reshape(b, n, -1)
        out = self.to_out(out)
        out = self.proj_drop(out)

        return out

class OfficialLinformer(nn.Module):
    def __init__(self, dim = 768, depth = 12, param_sharing = "layerwise", dim_k = 50, heads = 12, dim_head = None, drop_rate=0., attn_drop_rate=0., drop_path_rate=0., num_classes=1000, in_chans=3, img_size=224, patch_size=16, norm_layer=nn.LayerNorm, mlp_ratio=4., qkv_bias=False, qk_scale=None,
                 method="learnable", k_reduce_by_layer=0, checkpointing=False, dist_token=False):
        super().__init__()

        if param_sharing == "layerwise" or param_sharing == "kv":
            one_kv_head = True
            share_kv = True
        elif param_sharing == "headwise":
            one_kv_head = True
            share_kv = False
        else:
            one_kv_head = False
            share_kv = False

        self.embed_dim = dim
        self.num_classes = num_classes
        self.checkpointing = checkpointing

        self.head = nn.Linear(dim, num_classes) if num_classes > 0 else nn.Identity()

        seq_len = (img_size // patch_size) ** 2 + 1 if not dist_token else (img_size // patch_size) ** 2 + 2

        self.patch_embed = PatchEmbed(img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=dim)
        num_patches = self.patch_embed.num_patches

        self.cls_token = nn.Parameter(torch.zeros(1, 1, dim))
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 1, dim))
        self.pos_drop = nn.Dropout(p=drop_rate)

        E_proj_layerwise = None
        if param_sharing == "layerwise":
            E_proj_layerwise = get_EF((seq_len, dim_k), method)

        layers = nn.ModuleList([])
        for i in range(depth):
            attn = LinformerSelfAttention(method, dim, seq_len, k = max(1, dim_k - i * k_reduce_by_layer), heads = heads, dim_head = dim_head, one_kv_head = one_kv_head, share_kv = share_kv, attn_drop=attn_drop_rate, proj_drop=drop_rate, E_proj_layerwise = E_proj_layerwise, qkv_bias=qkv_bias, qk_scale=qk_scale)
            ff = FeedForward(dim, dropout = drop_rate, mult=mlp_ratio)

            layers.append(nn.ModuleList([
                PreNorm(dim, attn, norm_layer=norm_layer),
                PreNorm(dim, ff, norm_layer=norm_layer)
            ]))

        execute_type = SequentialSequence
        dpr = [x.item() for x in torch.linspace(0, drop_path_rate, depth)]  # stochastic depth decay rule
        self.net = execute_type(layers, layer_dropout = dpr)

        self.norm = norm_layer(dim)

        trunc_normal_(self.pos_embed, std=.02)
        trunc_normal_(self.cls_token, std=.02)
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)

    @torch.jit.ignore
    def no_weight_decay(self):
        return {'pos_embed', 'cls_token'}

    def forward(self, x):
        B = x.shape[0]
        x = self.patch_embed(x)

        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        x = torch.cat((cls_tokens, x), dim=1)
        x = x + self.pos_embed
        x = self.pos_drop(x)

        x = self.net(x)

        x = self.norm(x)
        x = x[:, 0]

        x = self.head(x)

        return x


class OfficialDistilledLinformer(OfficialLinformer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, dist_token=True)

        self.dist_token = nn.Parameter(torch.zeros(1, 1, self.embed_dim))
        num_patches = self.patch_embed.num_patches
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 2, self.embed_dim))
        self.head_dist = nn.Linear(self.embed_dim, self.num_classes) if self.num_classes > 0 else nn.Identity()

        trunc_normal_(self.dist_token, std=.02)
        trunc_normal_(self.pos_embed, std=.02)
        self.head_dist.apply(self._init_weights)

    def forward_features(self, x):
        # taken from https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
        # with slight modifications to add the dist_token
        B = x.shape[0]
        x = self.patch_embed(x)

        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        dist_token = self.dist_token.expand(B, -1, -1)
        x = torch.cat((cls_tokens, dist_token, x), dim=1)

        x = x + self.pos_embed
        x = self.pos_drop(x)

        if self.checkpointing:
            x = checkpoint(self.net, x)
        else:
            x = self.net(x)

        x = self.norm(x)
        return x[:, 0], x[:, 1]

    def forward(self, x):
        x, x_dist = self.forward_features(x)
        x = self.head(x)
        x_dist = self.head_dist(x_dist)
        if self.training:
            return x, x_dist
        else:
            # during inference, return the average of both classifier predictions
            return (x + x_dist) / 2
