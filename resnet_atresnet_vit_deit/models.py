# Copyright (c) 2015-present, Facebook, Inc.
# All rights reserved.
from functools import partial
from linformer import Linformer, DistilledLinformer # comment import of DistilledLinformer for "official" implementation
from official_linformer import OfficialLinformer, OfficialDistilledLinformer
from nystrom_transformer import Nystromformer
from timm.models.vision_transformer import VisionTransformer, _cfg, Mlp, DropPath
from timm.models.registry import register_model
from timm.models.layers import trunc_normal_
import torch.nn.functional as F
import torch
from torch import Tensor
import torch.nn as nn
from typing import Callable, List, Optional

__all__ = [
    'deit_tiny_patch16_224', 'deit_small_patch16_224', 'deit_base_patch16_224',
    'deit_tiny_distilled_patch16_224', 'deit_small_distilled_patch16_224',
    'deit_base_distilled_patch16_224', 'deit_base_patch16_384',
    'deit_base_distilled_patch16_384', 'resnet_attention18', 'resnet_attention34',
"resnet_scratch18", "resnet_pretrained18",
    "linformer_tiny_patch16_224", "linformer_tiny_distilled_patch16_224",
    "linformer_base_patch16_224", "linformer_base_distilled_patch16_224",
    "linformer_base_patch16_384", "linformer_base_distilled_patch16_384",
    "deit_tiny_patch16_384", "deit_tiny_distilled_patch16_384", "linformer_tiny_patch16_384", "linformer_tiny_distilled_patch16_384",
    "linformer_tiny_patch16_224_64heads", "linformer_tiny_distilled_patch16_224_64heads",
    "deit_tiny_patch16_224_64heads", "deit_tiny_distilled_patch16_224_64heads",
    "official_linformer_tiny_patch16_224", "official_linformer_tiny_distilled_patch16_224",
    "official_linformer_tiny_patch16_224_64heads", "official_linformer_tiny_distilled_patch16_224_64heads"
]


class Class_Attention(nn.Module):
    # taken from https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
    # with slight modifications to do CA
    def __init__(self, dim, dim_k, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
        super().__init__()
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = qk_scale or head_dim ** -0.5

        self.dim = dim
        self.q = nn.Linear(dim, dim, bias=qkv_bias)
        self.k = nn.Linear(dim_k, dim, bias=qkv_bias)
        self.v = nn.Linear(dim_k, dim, bias=qkv_bias)
        self.attn_drop = nn.Dropout(attn_drop)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(proj_drop)

    def forward(self, x, x_cls):
        B, N, C = x.shape
        q = self.q(x_cls).reshape(B, 1, self.num_heads, self.dim // self.num_heads).permute(0, 2, 1, 3)
        k = self.k(x).reshape(B, N, self.num_heads, self.dim // self.num_heads).permute(0, 2, 1, 3)

        q = q * self.scale
        v = self.v(x).reshape(B, N, self.num_heads, self.dim // self.num_heads).permute(0, 2, 1, 3)

        attn = (q @ k.transpose(-2, -1))
        attn = attn.softmax(dim=-1)
        attn = self.attn_drop(attn)

        x_cls = (attn @ v).transpose(1, 2).reshape(B, 1, self.dim)
        x_cls = self.proj(x_cls)
        x_cls = self.proj_drop(x_cls)

        return x_cls


class ResnetBlock(nn.Module):
    # taken from https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
    # with slight modifications to add CA and LayerScale
    def __init__(self, dim, dim_k, num_heads=8, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=nn.LayerNorm, Attention_block=Class_Attention,
                 Mlp_block=Mlp, init_values=1e-4):
        super().__init__()
        self.attn = Attention_block(
            dim, dim_k, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp_block(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

    def forward(self, x, x_cls):
        x_cls = x_cls + self.drop_path(self.attn(x, x_cls))

        x_cls = x_cls + self.drop_path(self.mlp(self.norm2(x_cls)))

        return x_cls


def conv1x1(in_planes: int, out_planes: int, stride: int = 1) -> nn.Conv2d:
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class ResNetAttention(nn.Module):

    def __init__(
            self,
            block,
            layers: List[int],
            image_size=224,
            num_classes: int = 10,
            zero_init_residual: bool = False,
            groups: int = 1,
            width_per_group: int = 64,
            replace_stride_with_dilation: Optional[List[bool]] = None,
            norm_layer: Optional[Callable[..., nn.Module]] = None,
            token_dim=256
    ) -> None:
        super().__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))
        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2,
                                       dilate=replace_stride_with_dilation[0])
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2,
                                       dilate=replace_stride_with_dilation[1])
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2,
                                       dilate=replace_stride_with_dilation[2])

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        self.token_dim = token_dim
        self.cls_token = nn.Parameter(torch.zeros(1, 1, token_dim))
        dim_first = (image_size - 1) // 2 + 1
        self.resnet_block1, dim = self._make_attention_layer(dim_first)
        self.resnet_block2, dim = self._make_attention_layer(dim)
        self.resnet_block3, dim = self._make_attention_layer(dim)
        self.resnet_block4, _ = self._make_attention_layer(dim)
        self.head = nn.Linear(token_dim, num_classes)

    def _make_attention_layer(self, last_size):
        dimension = (last_size - 1) // 2 + 1
        return ResnetBlock(self.token_dim, dimension*dimension), dimension

    def _make_layer(self, block, planes: int, blocks: int,
                    stride: int = 1, dilate: bool = False) -> nn.Sequential:
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.groups,
                            self.base_width, previous_dilation, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width, dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def _forward_impl(self, x: Tensor) -> Tensor:
        # See note [TorchScript super()]
        B, C, H, W = x.shape
        x_cls = self.cls_token.expand(B, -1, -1)
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x_cls = self.resnet_block1(x.reshape(B, 64, -1), x_cls)
        x = self.layer2(x)
        x_cls = self.resnet_block2(x.reshape(B, 128, -1), x_cls)
        x = self.layer3(x)
        x_cls = self.resnet_block3(x.reshape(B, 256, -1), x_cls)
        x = self.layer4(x)
        x_cls = self.resnet_block4(x.reshape(B, 512, -1), x_cls)

        return x_cls[:, 0]

    def forward(self, x: Tensor) -> Tensor:
        x_cls = self._forward_impl(x)
        return self.head(x_cls)


def iterative_inv(mat, n_iter=6):
    I = torch.eye(mat.size(-1), device=mat.device)
    K = mat

    # This is the exact coefficient computation, 1 / ||K||_1, of initialization of Z_0, leading to faster convergence.
    V = 1 / torch.max(torch.sum(K, dim=-2), dim=-1).values[:, :, None, None] * K.transpose(-1, -2)

    for _ in range(n_iter):
        KV = torch.matmul(K, V)
        V = torch.matmul(0.25 * V, 13 * I - torch.matmul(KV, 15 * I - torch.matmul(KV, 7 * I - KV)))
    return V


def forward(self, x):
    B, N, C = x.shape
    self.head_dim = C // self.num_heads
    qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
    q, k, v = qkv[0], qkv[1], qkv[2]  # make torchscript happy (cannot use tensor as tuple)
    q /= self.scale

    segs = N // self.num_landmarks
    if N % self.num_landmarks == 0:
        Q_landmarks = q.reshape(-1, self.num_heads, self.num_landmarks, segs,
                                self.head_dim).mean(dim=-2)
        K_landmarks = k.reshape(-1, self.num_heads, self.num_landmarks, segs,
                                self.head_dim).mean(dim=-2)
    else:
        num_k = (segs + 1) * self.num_landmarks - N
        keys_landmarks_f = k[:, :, :num_k * segs, :].reshape(B, self.num_heads, num_k, segs, self.head_dim).mean(
            dim=-2)
        keys_landmarks_l = k[:, :, num_k * segs:, :].reshape(B, self.num_heads, self.num_landmarks - num_k,
                                                             segs + 1,
                                                             self.head_dim).mean(dim=-2)
        K_landmarks = torch.cat((keys_landmarks_f, keys_landmarks_l), dim=-2)

        queries_landmarks_f = q[:, :, :num_k * segs, :].reshape(B, self.num_heads, num_k, segs, self.head_dim).mean(
            dim=-2)
        queries_landmarks_l = q[:, :, num_k * segs:, :].reshape(B, self.num_heads, self.num_landmarks - num_k,
                                                                segs + 1,
                                                                self.head_dim).mean(dim=-2)
        Q_landmarks = torch.cat((queries_landmarks_f, queries_landmarks_l), dim=-2)

    kernel_1 = F.softmax(torch.matmul(q, K_landmarks.transpose(-1, -2)), dim=-1)
    kernel_2 = F.softmax(torch.matmul(Q_landmarks, K_landmarks.transpose(-1, -2)), dim=-1)
    kernel_3 = F.softmax(torch.matmul(Q_landmarks, k.transpose(-1, -2)), dim=-1)
    x = torch.matmul(torch.matmul(kernel_1, self.iterative_inv(kernel_2)), torch.matmul(kernel_3, v))

    x = x.transpose(1, 2).reshape(B, N, C)
    x = self.proj(x)
    x = self.proj_drop(x)
    return x


class DistilledNystromformer(Nystromformer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dist_token = nn.Parameter(torch.zeros(1, 1, self.embed_dim))
        num_patches = self.patch_embed.num_patches
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 2, self.embed_dim))
        self.head_dist = nn.Linear(self.embed_dim, self.num_classes) if self.num_classes > 0 else nn.Identity()

        trunc_normal_(self.dist_token, std=.02)
        trunc_normal_(self.pos_embed, std=.02)
        self.head_dist.apply(self._init_weights)

    def forward_features(self, x):
        # taken from https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
        # with slight modifications to add the dist_token
        B = x.shape[0]
        x = self.patch_embed(x)

        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        dist_token = self.dist_token.expand(B, -1, -1)
        x = torch.cat((cls_tokens, dist_token, x), dim=1)

        x = x + self.pos_embed
        x = self.pos_drop(x)

        for blk in self.blocks:
            x = blk(x)

        x = self.norm(x)
        return x[:, 0], x[:, 1]

    def forward(self, x):
        x, x_dist = self.forward_features(x)
        x = self.head(x)
        x_dist = self.head_dist(x_dist)
        if self.training:
            return x, x_dist
        else:
            # during inference, return the average of both classifier predictions
            return (x + x_dist) / 2


class DistilledVisionTransformer(VisionTransformer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dist_token = nn.Parameter(torch.zeros(1, 1, self.embed_dim))
        num_patches = self.patch_embed.num_patches
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 2, self.embed_dim))
        self.head_dist = nn.Linear(self.embed_dim, self.num_classes) if self.num_classes > 0 else nn.Identity()

        trunc_normal_(self.dist_token, std=.02)
        trunc_normal_(self.pos_embed, std=.02)
        self.head_dist.apply(self._init_weights)

    def forward_features(self, x):
        # taken from https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
        # with slight modifications to add the dist_token
        B = x.shape[0]
        x = self.patch_embed(x)

        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        dist_token = self.dist_token.expand(B, -1, -1)
        x = torch.cat((cls_tokens, dist_token, x), dim=1)

        x = x + self.pos_embed
        x = self.pos_drop(x)

        for blk in self.blocks:
            x = blk(x)

        x = self.norm(x)
        return x[:, 0], x[:, 1]

    def forward(self, x):
        x, x_dist = self.forward_features(x)
        x = self.head(x)
        x_dist = self.head_dist(x_dist)
        if self.training:
            return x, x_dist
        else:
            # during inference, return the average of both classifier predictions
            return (x + x_dist) / 2


@register_model
def deit_tiny_patch16_224(pretrained=False, **kwargs):
    model = VisionTransformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_patch16_224-a1311bcf.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def deit_tiny_patch16_224_64heads(pretrained=False, **kwargs):
    model = VisionTransformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=64, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_patch16_224-a1311bcf.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model


@register_model
def deit_small_patch16_224(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = VisionTransformer(
            patch_size=16, embed_dim=384, depth=12, num_heads=6, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = Nystromformer(
            patch_size=16, embed_dim=384, depth=12, num_heads=6, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_small_patch16_224-cd65a155.pth",
            map_location="cpu", check_hash=True
        )
        model.load_state_dict(checkpoint["model"])
    return model


@register_model
def deit_base_patch16_224(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = VisionTransformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = Nystromformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="http://dl.fbaipublicfiles.com/deit/deit_base_patch16_224-b5f2ef4d.pth",
            map_location="cpu", check_hash=True
        )
        # model.load_state_dict(checkpoint["model"])
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model


@register_model
def deit_tiny_distilled_patch16_224(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = DistilledVisionTransformer(
            patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = DistilledNystromformer(
            pat=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_distilled_patch16_224-b40b3cf7.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def deit_tiny_distilled_patch16_224_64heads(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = DistilledVisionTransformer(
            patch_size=16, embed_dim=192, depth=12, num_heads=64, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = DistilledNystromformer(
            pat=16, embed_dim=192, depth=12, num_heads=64, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_distilled_patch16_224-b40b3cf7.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def deit_small_distilled_patch16_224(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = DistilledVisionTransformer(
            patch_size=16, embed_dim=384, depth=12, num_heads=6, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = DistilledNystromformer(
            patch_size=16, embed_dim=384, depth=12, num_heads=6, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_small_distilled_patch16_224-649709d9.pth",
            map_location="cpu", check_hash=True
        )
        model.load_state_dict(checkpoint["model"])
    return model


@register_model
def deit_base_distilled_patch16_224(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = DistilledVisionTransformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = DistilledNystromformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_base_distilled_patch16_224-df68dfff.pth",
            map_location="cpu", check_hash=True
        )
        # model.load_state_dict(checkpoint["model"])
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model


@register_model
def deit_base_patch16_384(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = VisionTransformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = Nystromformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_base_patch16_384-8de9b5d1.pth",
            map_location="cpu", check_hash=True
        )
        # model.load_state_dict(checkpoint["model"])
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k: v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model


@register_model
def deit_base_distilled_patch16_384(pretrained=False, **kwargs):
    if "num_landmarks" not in kwargs:
        model = DistilledVisionTransformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = DistilledNystromformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_base_distilled_patch16_384-d0272ac0.pth",
            map_location="cpu", check_hash=True
        )
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model

@register_model
def deit_tiny_patch16_384(pretrained=False, **kwargs):
    assert pretrained == False, "Pretrained model of this architecture is not available"

    model = VisionTransformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()

    return model

@register_model
def deit_tiny_distilled_patch16_384(pretrained=False, **kwargs):
    assert pretrained == False, "Pretrained model of this architecture is not available"

    if "num_landmarks" not in kwargs:
        model = DistilledVisionTransformer(
            patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    else:
        model = DistilledNystromformer(
            pat=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()

    return model

from torchvision.models.resnet import BasicBlock
from torch.hub import load_state_dict_from_url

@register_model
def resnet_attention18(pretrained=True, **kwargs):
    model = ResNetAttention(BasicBlock, [2, 2, 2, 2])
    if pretrained:
        state_dict = load_state_dict_from_url('https://download.pytorch.org/models/resnet18-5c106cde.pth')
        model.load_state_dict(state_dict, strict=False)
    return model


@register_model
def resnet_attention34(pretrained=True, **kwargs):
    model = ResNetAttention(BasicBlock, [3, 4, 6, 3])
    if pretrained:
        state_dict = load_state_dict_from_url('https://download.pytorch.org/models/resnet18-5c106cde.pth')
        model.load_state_dict(state_dict, strict=False)
    return model

# class ResNet is coded to be as similar as possible to ResNetAttention class - for comparison
class ResNet(nn.Module):

    def __init__(
        self,
        block,
        layers: List[int],
        num_classes: int = 10,
        zero_init_residual: bool = False,
        groups: int = 1,
        width_per_group: int = 64,
        replace_stride_with_dilation: Optional[List[bool]] = None,
        norm_layer: Optional[Callable[..., nn.Module]] = None
    ) -> None:
        super(ResNet, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))
        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2,
                                       dilate=replace_stride_with_dilation[0])
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2,
                                       dilate=replace_stride_with_dilation[1])
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2,
                                       dilate=replace_stride_with_dilation[2])
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(self, block, planes: int, blocks: int,
                    stride: int = 1, dilate: bool = False) -> nn.Sequential:
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.groups,
                            self.base_width, previous_dilation, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width, dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def _forward_impl(self, x: Tensor) -> Tensor:
        # See note [TorchScript super()]
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)

        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)

@register_model
def resnet_scratch18(pretrained=False, **kwargs):
    model = ResNet(BasicBlock, [2, 2, 2, 2], num_classes=10)
    return model

@register_model
def resnet_pretrained18(pretrained=True, **kwargs):
    model = ResNet(BasicBlock, [2, 2, 2, 2], num_classes=10)

    # model = ResNet(BasicBlock, [2, 2, 2, 2])
    # num_ftrs = model.fc.in_features
    # model.fc = nn.Linear(num_ftrs, 10)

    # model = torchmodels.resnet18(pretrained=True, num_classes=10)

    if pretrained:
        state_dict = load_state_dict_from_url('https://download.pytorch.org/models/resnet18-5c106cde.pth')
        state_dict = {k: v for k, v in state_dict.items() if k not in {"fc.weight", "fc.bias"}}
        model.load_state_dict(state_dict, strict=False)

    return model

#### LINFORMER ####
@register_model
def linformer_tiny_patch16_224(pretrained=False, **kwargs):
    # official
    # model = Linformer(patch_size=16, dim=192, depth=12, heads = 3, dim_head = 64, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    # my
    model = Linformer(patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)

    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_patch16_224-a1311bcf.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def official_linformer_tiny_patch16_224(pretrained=False, **kwargs):
    model = OfficialLinformer(patch_size=16, dim=192, depth=12, heads = 3, dim_head = 64, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)

    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_patch16_224-a1311bcf.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def linformer_tiny_distilled_patch16_224(pretrained=False, **kwargs):
    model = DistilledLinformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_distilled_patch16_224-b40b3cf7.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def official_linformer_tiny_distilled_patch16_224(pretrained=False, **kwargs):
    model = OfficialDistilledLinformer(patch_size=16, dim=192, depth=12, heads = 3, dim_head = 64, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_distilled_patch16_224-b40b3cf7.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def linformer_base_patch16_224(pretrained=False, **kwargs):
    model = Linformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="http://dl.fbaipublicfiles.com/deit/deit_base_patch16_224-b5f2ef4d.pth",
            map_location="cpu", check_hash=True
        )
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model

@register_model
def linformer_base_distilled_patch16_224(pretrained=False, **kwargs):
    model = DistilledLinformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_base_distilled_patch16_224-df68dfff.pth",
            map_location="cpu", check_hash=True
        )
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model

@register_model
def linformer_base_patch16_384(pretrained=False, **kwargs):
    model = Linformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_base_patch16_384-8de9b5d1.pth",
            map_location="cpu", check_hash=True
        )
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k: v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model

@register_model
def linformer_base_distilled_patch16_384(pretrained=False, **kwargs):
    model = DistilledLinformer(
            patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True,
            norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_base_distilled_patch16_384-d0272ac0.pth",
            map_location="cpu", check_hash=True
        )
        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)
    return model

@register_model
def linformer_tiny_patch16_384(pretrained=False, **kwargs):
    assert pretrained == False, "Pretrained model of this architecture is not available"

    model = Linformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()

    return model

@register_model
def linformer_tiny_distilled_patch16_384(pretrained=False, **kwargs):
    assert pretrained == False, "Pretrained model of this architecture is not available"

    model = DistilledLinformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=3, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()

    return model

@register_model
def linformer_tiny_patch16_224_64heads(pretrained=False, **kwargs):
    # official
    # model = Linformer(patch_size=16, dim=192, depth=12, heads=64, dim_head=3, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    # my
    model = Linformer(patch_size=16, embed_dim=192, depth=12, num_heads=64, mlp_ratio=4, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_patch16_224-a1311bcf.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def official_linformer_tiny_patch16_224_64heads(pretrained=False, **kwargs):
    model = OfficialLinformer(patch_size=16, dim=192, depth=12, heads=64, dim_head=3, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_patch16_224-a1311bcf.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def linformer_tiny_distilled_patch16_224_64heads(pretrained=False, **kwargs):
    model = DistilledLinformer(
        patch_size=16, embed_dim=192, depth=12, num_heads=64, mlp_ratio=4, qkv_bias=True,
        norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_distilled_patch16_224-b40b3cf7.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model

@register_model
def official_linformer_tiny_distilled_patch16_224_64heads(pretrained=False, **kwargs):
    model = OfficialDistilledLinformer(patch_size=16, dim=192, depth=12, heads=64, dim_head=3, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), **kwargs)
    model.default_cfg = _cfg()
    if pretrained:
        checkpoint = torch.hub.load_state_dict_from_url(
            url="https://dl.fbaipublicfiles.com/deit/deit_tiny_distilled_patch16_224-b40b3cf7.pth",
            map_location="cpu", check_hash=True
        )

        ckpt_state_dict = checkpoint["model"]
        ckpt_state_dict = {k:v for k, v in ckpt_state_dict.items() if k not in {"head.weight", "head.bias", "head_dist.weight", "head_dist.bias"}}
        model.load_state_dict(ckpt_state_dict, strict=False)

    return model