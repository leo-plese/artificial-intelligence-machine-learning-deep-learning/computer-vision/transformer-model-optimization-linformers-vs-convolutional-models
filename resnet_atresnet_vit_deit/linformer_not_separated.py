import torch
import torch.nn as nn
from timm.models.vision_transformer import Mlp, HybridEmbed, PatchEmbed
from timm.models.layers import DropPath, trunc_normal_
from torch.utils.checkpoint import checkpoint
from math import sqrt

# MY implementation (Leo Plese)

# NOTE - difference linformer.py <-> linformer_not_separated.py:
# linformer.py: special tokens (class, distillation) are projected using E, F tensors together with all other tokens
# linformer_not_separated.py: special tokens (class, distillation) are NOT projected using E, F tensors together with all other tokens - they are separated before the projection and concatenated after the projection unchanged

def get_EF(dims,method="learnable"):
    """
    Retuns the E or F matrix, initialized via xavier initialization.
    This is the recommended way to do it according to the authors of the paper.
    Includes a method for no additional params.
    """
    if method == "no_params":
        mat = torch.zeros(dims).to(device="cuda")
        torch.nn.init.normal_(mat, mean=0.0, std=1 / dims[1])
        return mat

    learnable_param = nn.Parameter(torch.zeros(dims).to(device="cuda"))
    torch.nn.init.kaiming_uniform_(learnable_param, a=sqrt(5))

    return learnable_param

class LinformerAttention(nn.Module):
    def __init__(self, seq_len, dim_k, one_kv_head, share_kv, method, E_proj_layerwise,
                 dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
        super().__init__()

        self.share_kv = share_kv

        self.head_dim = dim // num_heads
        self.dim_k = dim_k

        self.E_proj = get_EF((seq_len, dim_k), method) if E_proj_layerwise is None else E_proj_layerwise

        self.num_heads = num_heads

        # NOTE scale factor was wrong in my original version, can set manually to be compat with prev weights
        self.scale = qk_scale or self.head_dim ** -0.5

        kv_dim = self.head_dim if one_kv_head else dim

        self.to_q = nn.Linear(dim, dim, bias = qkv_bias)
        self.to_k = nn.Linear(dim, kv_dim, bias = qkv_bias)

        if not share_kv:
            self.F_proj = get_EF((seq_len, dim_k), method)
            self.to_v = nn.Linear(dim, kv_dim, bias = qkv_bias)

        self.attn_drop = nn.Dropout(attn_drop)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(proj_drop)

    def forward(self, x):
        B, N, _ = x.shape
        q = self.to_q(x).reshape(B, N, self.num_heads, -1).transpose(1, 2)
        k = self.to_k(x)
        v = k if self.share_kv else self.to_v(x)

        proj_seq_len = lambda args: torch.einsum('bnd,nk->bkd', *args)

        kv_projs = (self.E_proj, self.F_proj if not self.share_kv else self.E_proj)

        k, v = map(proj_seq_len, zip((k, v), kv_projs))

        merge_key_values = lambda t: t.reshape(B, self.dim_k, -1, self.head_dim).transpose(1, 2).expand(-1, self.num_heads, -1, -1)

        k, v = map(merge_key_values, (k, v))

        attn = torch.einsum("bhnd,bhkd->bhnk", q, k) * self.scale
        attn = attn.softmax(dim=-1)
        attn = self.attn_drop(attn)

        x = torch.einsum('bhnk,bhkd->bhnd', attn, v).transpose(1, 2).reshape(B, N, -1)
        x = self.proj(x)
        x = self.proj_drop(x)
        return x

class Block(nn.Module):

    def __init__(self, seq_len, dim_k, one_kv_head, share_kv, method, E_proj_layerwise, dim, num_heads,
                 mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=nn.LayerNorm):
        super().__init__()

        self.attn = LinformerAttention(seq_len, dim_k, one_kv_head, share_kv, method, E_proj_layerwise,
                                       dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)

        self.norm1 = norm_layer(dim)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

    def forward(self, x):
        x = x + self.drop_path(self.attn(self.norm1(x)))
        x = x + self.drop_path(self.mlp(self.norm2(x)))

        return x


class Linformer(nn.Module):
    # Note: if param_sharing="layerwise", argument k_reduce_by_layer has no effect (dim_k is same for all layers)
    def __init__(self, dim_k=50, param_sharing="layerwise", method="learnable", k_reduce_by_layer=0,
                 checkpointing=False, img_size=224, patch_size=16, in_chans=3, num_classes=1000, embed_dim=768,
                 depth=12,
                 num_heads=12, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop_rate=0., attn_drop_rate=0.,
                 drop_path_rate=0., hybrid_backbone=None, norm_layer=nn.LayerNorm, dist_token=False):
        super().__init__()
        assert param_sharing == "none" or param_sharing == "headwise" or param_sharing == "kv" or param_sharing == "layerwise", "The `parameter_sharing` flag has to be either 'none', 'headwise', 'kv', or 'layerwise'."
        assert method == "learnable" or method == "no_params", "The method flag needs to be either 'learnable' or 'no_params'!"

        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models

        seq_len = (img_size // patch_size) ** 2 + 1 if not dist_token else (img_size // patch_size) ** 2 + 2

        E_proj_layerwise = None
        if param_sharing == "layerwise":
            E_proj_layerwise = get_EF((seq_len, dim_k), method)

        self.checkpointing = checkpointing

        if hybrid_backbone is not None:
            self.patch_embed = HybridEmbed(
                hybrid_backbone, img_size=img_size, in_chans=in_chans, embed_dim=embed_dim)
        else:
            self.patch_embed = PatchEmbed(
                img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=embed_dim)
        num_patches = self.patch_embed.num_patches

        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 1, embed_dim))
        self.pos_drop = nn.Dropout(p=drop_rate)

        if param_sharing == "layerwise" or param_sharing == "kv":
            one_kv_head = True
            share_kv = True
        elif param_sharing == "headwise":
            one_kv_head = True
            share_kv = False
        else:
            one_kv_head = False
            share_kv = False

        dpr = [x.item() for x in torch.linspace(0, drop_path_rate, depth)]  # stochastic depth decay rule
        self.blocks = nn.ModuleList([
            Block(
                seq_len, max(1, dim_k - i * k_reduce_by_layer), one_kv_head, share_kv, method, E_proj_layerwise=E_proj_layerwise,
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                qk_scale=qk_scale,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=dpr[i], norm_layer=norm_layer)
            for i in range(depth)])
        self.norm = norm_layer(embed_dim)

        # Classifier head
        self.head = nn.Linear(embed_dim, num_classes) if num_classes > 0 else nn.Identity()

        trunc_normal_(self.pos_embed, std=.02)
        trunc_normal_(self.cls_token, std=.02)
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)

    @torch.jit.ignore
    def no_weight_decay(self):
        return {'pos_embed', 'cls_token'}

    def get_classifier(self):
        return self.head

    def reset_classifier(self, num_classes, global_pool=''):
        self.num_classes = num_classes
        self.head = nn.Linear(self.embed_dim, num_classes) if num_classes > 0 else nn.Identity()

    def forward_features(self, x):
        B = x.shape[0]
        x = self.patch_embed(x)

        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        x = torch.cat((cls_tokens, x), dim=1)
        x = x + self.pos_embed
        x = self.pos_drop(x)

        if self.checkpointing:
            for blk in self.blocks:
                x = checkpoint(blk, x)
        else:
            for blk in self.blocks:
                x = blk(x)

        x = self.norm(x)
        return x[:, 0]

    def forward(self, x):
        x = self.forward_features(x)
        x = self.head(x)
        return x


class DistilledLinformer(Linformer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, dist_token=True)

        self.dist_token = nn.Parameter(torch.zeros(1, 1, self.embed_dim))
        num_patches = self.patch_embed.num_patches
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 2, self.embed_dim))
        self.head_dist = nn.Linear(self.embed_dim, self.num_classes) if self.num_classes > 0 else nn.Identity()

        trunc_normal_(self.dist_token, std=.02)
        trunc_normal_(self.pos_embed, std=.02)
        self.head_dist.apply(self._init_weights)

    def forward_features(self, x):
        # taken from https://github.com/rwightman/pytorch-image-models/blob/master/timm/models/vision_transformer.py
        # with slight modifications to add the dist_token
        B = x.shape[0]
        x = self.patch_embed(x)

        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        dist_token = self.dist_token.expand(B, -1, -1)
        x = torch.cat((cls_tokens, dist_token, x), dim=1)

        x = x + self.pos_embed
        x = self.pos_drop(x)

        if self.checkpointing:
            for blk in self.blocks:
                x = checkpoint(blk, x)
        else:
            for blk in self.blocks:
                x = blk(x)

        x = self.norm(x)
        return x[:, 0], x[:, 1]

    def forward(self, x):
        x, x_dist = self.forward_features(x)
        x = self.head(x)
        x_dist = self.head_dist(x_dist)
        if self.training:
            return x, x_dist
        else:
            # during inference, return the average of both classifier predictions
            return (x + x_dist) / 2
