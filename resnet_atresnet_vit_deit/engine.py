# Copyright (c) 2015-present, Facebook, Inc.
# All rights reserved.
"""
Train and eval functions used in main.py
"""
import math
import sys
from typing import Iterable, Optional
from sklearn.metrics import confusion_matrix
import numpy as np

import torch

from timm.data import Mixup
from timm.utils import accuracy, get_state_dict, ModelEma

from losses import DistillationLoss
import utils

from samplers import RASampler
from math import ceil

def train_one_epoch_ckpt_within_epoch(out_ckpt_dir, lr_scheduler, args, model: torch.nn.Module, criterion: DistillationLoss,
                                      data_loader: Iterable, optimizer: torch.optim.Optimizer,
                                      device: torch.device, epoch: int, loss_scaler, max_norm: float = 0,
                                      model_ema: Optional[ModelEma] = None, mixup_fn: Optional[Mixup] = None,
                                      set_training_mode=True, start_iteration=0, ckpt_freq=500):
    model.train(set_training_mode)
    metric_logger = utils.MetricLogger(delimiter="  ",calculate_total_stats=False)
    metric_logger.add_meter('lr', utils.SmoothedValue(window_size=1, fmt='{value:.6f}'))
    header = 'Epoch: [{}]'.format(epoch)
    print_freq = 10

    if start_iteration == 0:
        mod_val = 0
    elif ckpt_freq == (start_iteration-1):
        mod_val = 0
    elif ckpt_freq < (start_iteration-1):
        mod_val = (start_iteration-1) % ckpt_freq
    else:   # ckpt_freq > start_iteration
        mod_val = (start_iteration-1)

    iter_num = start_iteration
    for samples, targets in metric_logger.log_every(data_loader, print_freq, header, start_iteration):
        samples = samples.to(device, non_blocking=True)
        targets = targets.to(device, non_blocking=True)

        if mixup_fn is not None:
            samples, targets = mixup_fn(samples, targets)

        with torch.cuda.amp.autocast():
            outputs = model(samples)
            loss = criterion(samples, outputs, targets)

        loss_value = loss.item()

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value))
            sys.exit(1)

        optimizer.zero_grad()

        # this attribute is added by timm on one optimizer (adahessian)
        is_second_order = hasattr(optimizer, 'is_second_order') and optimizer.is_second_order
        loss_scaler(loss, optimizer, clip_grad=max_norm,
                    parameters=model.parameters(), create_graph=is_second_order)

        torch.cuda.synchronize()
        if model_ema is not None:
            model_ema.update(model)

        metric_logger.update(loss=loss_value)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])

        if iter_num % ckpt_freq == mod_val:
            ckpt_name = "checkpoint_iter_" + str(iter_num) + ".pth"
            checkpoint_paths = [out_ckpt_dir / ckpt_name]
            for checkpoint_path in checkpoint_paths:
                utils.save_on_master({
                    # "data_loader" : data_loader,
                    'model': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    'lr_scheduler': lr_scheduler.state_dict(),
                    'epoch': epoch,
                    "iteration":iter_num,
                    'model_ema': get_state_dict(model_ema),
                    'scaler': loss_scaler.state_dict(),
                    'args': args,
                }, checkpoint_path)
            # torch.save(data_loader,out_ckpt_dir / "dataloader.pth")

        iter_num += 1

def train_one_epoch(model: torch.nn.Module, criterion: DistillationLoss,
                    data_loader: Iterable, optimizer: torch.optim.Optimizer,
                    device: torch.device, epoch: int, loss_scaler, max_norm: float = 0,
                    model_ema: Optional[ModelEma] = None, mixup_fn: Optional[Mixup] = None,
                    set_training_mode=True):
    model.train(set_training_mode)
    metric_logger = utils.MetricLogger(delimiter="  ")
    metric_logger.add_meter('lr', utils.SmoothedValue(window_size=1, fmt='{value:.6f}'))
    header = 'Epoch: [{}]'.format(epoch)
    print_freq = 10

    for samples, targets in metric_logger.log_every(data_loader, print_freq, header):
        samples = samples.to(device, non_blocking=True)
        targets = targets.to(device, non_blocking=True)

        if mixup_fn is not None:
            samples, targets = mixup_fn(samples, targets)

        with torch.cuda.amp.autocast():
            outputs = model(samples)
            loss = criterion(samples, outputs, targets)

        loss_value = loss.item()

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value))
            sys.exit(1)

        optimizer.zero_grad()

        # this attribute is added by timm on one optimizer (adahessian)
        is_second_order = hasattr(optimizer, 'is_second_order') and optimizer.is_second_order
        loss_scaler(loss, optimizer, clip_grad=max_norm,
                    parameters=model.parameters(), create_graph=is_second_order)

        torch.cuda.synchronize()
        if model_ema is not None:
            model_ema.update(model)

        metric_logger.update(loss=loss_value)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])
    # gather the stats from all processes
    metric_logger.synchronize_between_processes()
    print("Averaged stats:", metric_logger)
    return {k: meter.global_avg for k, meter in metric_logger.meters.items()}


@torch.no_grad()
def evaluate(data_loader, model, device):
    criterion = torch.nn.CrossEntropyLoss()

    metric_logger = utils.MetricLogger(delimiter="  ")
    header = 'Test:'

    print("data_loader.batch_size =", data_loader.batch_size)

    print("mem allocated BEFORE inference:", torch.cuda.memory_allocated(), torch.cuda.max_memory_allocated())

    # switch to evaluation mode
    model.eval()

    conf_mat = np.zeros((10, 10))
    cls_labs = [i for i in range(10)]
    for images, target in metric_logger.log_every(data_loader, 10, header):
        images = images.to(device, non_blocking=True)
        target = target.to(device, non_blocking=True)

        # compute output
        with torch.cuda.amp.autocast():
            output = model(images)
            conf_mat += confusion_matrix(target.cpu().numpy(), output.argmax(dim=-1).cpu().numpy(), labels=cls_labs)
            loss = criterion(output, target)

        acc1, acc5 = accuracy(output, target, topk=(1, 5))

        batch_size = images.shape[0]
        metric_logger.update(loss=loss.item())
        metric_logger.meters['acc1'].update(acc1.item(), n=batch_size)
        metric_logger.meters['acc5'].update(acc5.item(), n=batch_size)

    # gather the stats from all processes
    metric_logger.synchronize_between_processes()
    print('* Acc@1 {top1.global_avg:.3f} Acc@5 {top5.global_avg:.3f} loss {losses.global_avg:.3f}'
          .format(top1=metric_logger.acc1, top5=metric_logger.acc5, losses=metric_logger.loss))

    return {k: meter.global_avg for k, meter in metric_logger.meters.items()}, conf_mat


def model_inference(model, dataloader, n=10000):
    from time import perf_counter
    device = torch.device("cuda")

    model.eval()
    model.to(device)
    with torch.no_grad():
        input = next(iter(dataloader))[0].to(device)
        logits = model(input)
        torch.cuda.synchronize()
        t0 = perf_counter()
        n_steps = ceil(n/dataloader.batch_size)
        print("n_steps =",n_steps)
        for _ in range(n_steps):
            input = next(iter(dataloader))[0].to(device)
            logits = model(input)
            _, pred = logits.max(1)
            out = pred.data.byte().cpu()
        torch.cuda.synchronize()
        t1 = perf_counter()
        fps = n / (t1 - t0)

        return fps

def model_inference_deit_way(model, dataloader, n=30):
    import time
    device = torch.device("cuda")

    model.eval()
    model.to(device)

    warm_up = 10
    torch.cuda.empty_cache()

    torch.cuda.synchronize()
    with torch.no_grad():
        for _ in range(warm_up):
            x = next(iter(dataloader))[0].to(device)
            y = model(x)
            y = torch.softmax(y, 1)

    start = time.time()
    with torch.no_grad():
        for _ in range(n):
            x = next(iter(dataloader))[0].to(device)
            y = model(x)
            y = torch.softmax(y, 1)
    torch.cuda.synchronize()

    end = time.time()

    return (dataloader.batch_size * n) / (end - start)


def do_train_step(num_tasks, global_rank, cur_bs, args, dataset_train, model: torch.nn.Module, criterion: DistillationLoss,
                      optimizer: torch.optim.Optimizer,
                    device: torch.device, loss_scaler, max_norm: float = 0,
                    model_ema: Optional[ModelEma] = None, mixup_fn: Optional[Mixup] = None):
    torch.cuda.empty_cache()
    sampler_train = RASampler(
        dataset_train, num_replicas=num_tasks, rank=global_rank, shuffle=True
    )

    data_loader = torch.utils.data.DataLoader(
        dataset_train, sampler=sampler_train,
        batch_size=cur_bs,
        num_workers=args.num_workers,
        pin_memory=args.pin_mem,
        drop_last=True,
    )

    samples, targets = next(iter(data_loader))
    samples = samples.to(device, non_blocking=True)
    targets = targets.to(device, non_blocking=True)

    if mixup_fn is not None:
        samples, targets = mixup_fn(samples, targets)

    with torch.cuda.amp.autocast():
        outputs = model(samples)
        loss = criterion(samples, outputs, targets)

    loss_value = loss.item()

    if not math.isfinite(loss_value):
        print("Loss is {}, stopping training".format(loss_value))
        sys.exit(1)

    optimizer.zero_grad()

    # this attribute is added by timm on one optimizer (adahessian)
    is_second_order = hasattr(optimizer, 'is_second_order') and optimizer.is_second_order
    loss_scaler(loss, optimizer, clip_grad=max_norm,
                parameters=model.parameters(), create_graph=is_second_order)

    torch.cuda.synchronize()
    if model_ema is not None:
        model_ema.update(model)

def do_eval(model, bs, input_size):
    torch.cuda.empty_cache()

    x = torch.randn(bs, 3, input_size, input_size).cuda()

    with torch.no_grad():
        y = model(x)
        y = torch.softmax(y, 1)

def get_max_batchsize(args, dataset_val, model: torch.nn.Module, device: torch.device):
    model.eval()

    input_size = args.input_size
    cur_bs = 2
    while True:
        try:
            print("cur_bs =",cur_bs)
            do_eval(model, cur_bs, input_size)

            cur_bs *= 2
        except RuntimeError as err:
            print("!!! err =",err)
            top_bs = cur_bs
            bottom_bs = cur_bs//2
            break

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    while (top_bs - bottom_bs) > 1:
        mid_bs = (top_bs+bottom_bs)//2
        print("mid_bs =",mid_bs)
        try:
            do_eval(model, mid_bs, input_size)
            bottom_bs = mid_bs
        except RuntimeError:
            top_bs = mid_bs

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    try:
        do_eval(model, top_bs, input_size)
        max_bs = top_bs
    except RuntimeError as err:
        print("!!! err =", err)
        max_bs = bottom_bs

    return max_bs

def measure_speed_wo_loading(model, bs, input_size, n=30):
    import torch
    import time

    model.eval()
    model.to(torch.device("cuda"))

    warm_up = 10
    torch.cuda.empty_cache()
    x = torch.randn(bs, 3, input_size, input_size).cuda()

    torch.cuda.synchronize()
    with torch.no_grad():
        for _ in range(warm_up):
            y = model(x)
            y = torch.softmax(y, 1)

    start = time.time()
    with torch.no_grad():
        for _ in range(n):
            y = model(x)
            y = torch.softmax(y, 1)
    torch.cuda.synchronize()

    end = time.time()
    return (bs * n) / (end - start)

def get_train_max_batchsize(args, dataset_train, model: torch.nn.Module, criterion: DistillationLoss,
                      optimizer: torch.optim.Optimizer,
                      device: torch.device, loss_scaler, max_norm: float = 0,
                      model_ema: Optional[ModelEma] = None, mixup_fn: Optional[Mixup] = None,
                      set_training_mode=True):
    model.train(set_training_mode)

    num_tasks = utils.get_world_size()
    global_rank = utils.get_rank()

    cur_bs = 2
    while True:
        try:
            print("cur_bs =",cur_bs)
            do_train_step(num_tasks, global_rank, cur_bs, args, dataset_train, model, criterion,optimizer,device, loss_scaler, max_norm,model_ema, mixup_fn)

            cur_bs *= 2
        except RuntimeError as err:
            print("!!! err =",err)
            top_bs = cur_bs
            bottom_bs = cur_bs//2
            break

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    while (top_bs - bottom_bs) > 1:
        mid_bs = (top_bs+bottom_bs)//2
        print("mid_bs =",mid_bs)
        if mid_bs % 2 != 0:
            break
        try:
            do_train_step(num_tasks, global_rank, mid_bs, args, dataset_train, model, criterion,
                          optimizer,
                          device, loss_scaler, max_norm,
                          model_ema, mixup_fn)
            bottom_bs = mid_bs
        except RuntimeError:
            top_bs = mid_bs

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    try:
        do_train_step(num_tasks, global_rank, top_bs, args, dataset_train, model, criterion,
                      optimizer,
                      device, loss_scaler, max_norm,
                      model_ema, mixup_fn)
        max_bs = top_bs
    except RuntimeError as err:
        print("!!! err =", err)
        max_bs = bottom_bs

    return max_bs