import torch
import torch.nn as nn
import torch.nn.functional as F
import torchsummary
from math import sqrt

def get_EF(dims,method="learnable"):
    if method == "no_params":
        mat = torch.zeros(dims).to(device="cuda")
        torch.nn.init.normal_(mat, mean=0.0, std=1 / dims[1])
        return mat

    learnable_param = nn.Parameter(torch.zeros(dims).to(device="cuda"))
    torch.nn.init.kaiming_uniform_(learnable_param, a=sqrt(5))

    return learnable_param


class TransformerEncoder(nn.Module):
    def __init__(self, feats:int, mlp_hidden:int, head:int=8, dropout:float=0.):
        super(TransformerEncoder, self).__init__()
        self.la1 = nn.LayerNorm(feats)
        self.msa = MultiHeadSelfAttention(feats, head=head, dropout=dropout)
        self.la2 = nn.LayerNorm(feats)
        self.mlp = nn.Sequential(
            nn.Linear(feats, mlp_hidden),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(mlp_hidden, feats),
            nn.GELU(),
            nn.Dropout(dropout),
        )

    def forward(self, x):
        out = self.msa(self.la1(x)) + x
        out = self.mlp(self.la2(out)) + out
        return out


class MultiHeadSelfAttention(nn.Module):
    def __init__(self, feats:int, head:int=8, dropout:float=0.):
        super(MultiHeadSelfAttention, self).__init__()
        self.head = head
        self.feats = feats
        self.sqrt_d = self.feats**0.5

        self.q = nn.Linear(feats, feats)
        self.k = nn.Linear(feats, feats)
        self.v = nn.Linear(feats, feats)

        self.o = nn.Linear(feats, feats)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        b, n, f = x.size()
        q = self.q(x).view(b, n, self.head, self.feats//self.head).transpose(1,2)
        k = self.k(x).view(b, n, self.head, self.feats//self.head).transpose(1,2)
        v = self.v(x).view(b, n, self.head, self.feats//self.head).transpose(1,2)

        score = F.softmax(torch.einsum("bhif, bhjf->bhij", q, k)/self.sqrt_d, dim=-1) #(b,h,n,n)
        attn = torch.einsum("bhij, bhjf->bihf", score, v) #(b,n,h,f//h)
        o = self.dropout(self.o(attn.flatten(2)))
        return o


class LinformerEncoder(nn.Module):
    def __init__(self, num_tokens, dim_k, one_kv_head, share_kv, method, E_proj_layerwise, feats:int, mlp_hidden:int, head:int=8, dropout:float=0.):
        super(LinformerEncoder, self).__init__()
        self.la1 = nn.LayerNorm(feats)
        self.msa = LinformerAttention(num_tokens, dim_k, one_kv_head, share_kv, method, E_proj_layerwise, feats, head, dropout=dropout)
        self.la2 = nn.LayerNorm(feats)
        self.mlp = nn.Sequential(
            nn.Linear(feats, mlp_hidden),
            nn.GELU(),
            nn.Dropout(dropout),
            nn.Linear(mlp_hidden, feats),
            nn.GELU(),
            nn.Dropout(dropout),
        )

    def forward(self, x):
        out = self.msa(self.la1(x)) + x
        out = self.mlp(self.la2(out)) + out
        return out

class LinformerAttention(nn.Module):
    def __init__(self, seq_len, dim_k, one_kv_head, share_kv, method, E_proj_layerwise,
                 feats, head=8, dropout:float=0.):
        super().__init__()

        self.share_kv = share_kv

        self.head_dim = feats // head
        self.dim_k = dim_k

        self.E_proj = get_EF((seq_len, dim_k), method) if E_proj_layerwise is None else E_proj_layerwise

        self.head = head

        self.sqrt_d = feats ** 0.5

        kv_dim = self.head_dim if one_kv_head else feats

        self.to_q = nn.Linear(feats, feats)
        self.to_k = nn.Linear(feats, kv_dim)

        if not share_kv:
            self.F_proj = get_EF((seq_len, dim_k), method)
            self.to_v = nn.Linear(feats, kv_dim)

        self.proj = nn.Linear(feats, feats)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        B, N, _ = x.shape
        q = self.to_q(x).reshape(B, N, self.head, -1).transpose(1, 2)
        k = self.to_k(x)
        v = k if self.share_kv else self.to_v(x)

        proj_seq_len = lambda args: torch.einsum('bnd,nk->bkd', *args)

        kv_projs = (self.E_proj, self.F_proj if not self.share_kv else self.E_proj)

        k, v = map(proj_seq_len, zip((k, v), kv_projs))

        merge_key_values = lambda t: t.reshape(B, self.dim_k, -1, self.head_dim).transpose(1, 2).expand(-1, self.head, -1, -1)

        k, v = map(merge_key_values, (k, v))

        attn = torch.einsum("bhnd,bhkd->bhnk", q, k) / self.sqrt_d
        attn = attn.softmax(dim=-1)

        x = torch.einsum('bhnk,bhkd->bhnd', attn, v).transpose(1, 2).reshape(B, N, -1)
        x = self.proj(x)
        x = self.dropout(x)
        return x

class MultiHeadDepthwiseSelfAttention(nn.Module):
    def __init__(self, feats:int, head:int=8, dropout:float=0):
        super(MultiHeadDepthwiseSelfAttention, self).__init__()
        ...

    def forward(self, x):
        
        ...

if __name__=="__main__":
    b,n,f = 4, 16, 128
    x = torch.randn(b,n,f)
    # net = MultiHeadSelfAttention(f)
    net = TransformerEncoder(f)
    torchsummary.summary(net, (n,f))
    # out = net(x)
    # print(out.shape)



