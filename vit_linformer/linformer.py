import torch
import torch.nn as nn
from torch.utils.checkpoint import checkpoint

from layers import LinformerEncoder, get_EF

class Linformer(nn.Module):
    def __init__(self, in_c:int=3, num_classes:int=10, img_size:int=32, patch:int=8, dropout:float=0., num_layers:int=7, hidden:int=384, mlp_hidden:int=384*4, head:int=8, is_cls_token:bool=True,
                 dim_k=50, param_sharing="layerwise", method="learnable", k_reduce_by_layer=0, checkpointing=False):
        super(Linformer, self).__init__()
        # hidden=384

        print("dim_k",dim_k)
        print("param_sharing", param_sharing)
        print("method", method)
        print("k_reduce_by_layer", k_reduce_by_layer)
        print("checkpointing", checkpointing)


        self.patch = patch # number of patches in one row(or col)
        self.is_cls_token = is_cls_token
        self.patch_size = img_size//self.patch
        f = (img_size//self.patch)**2*3 # 48 # patch vec length
        num_tokens = (self.patch**2)+1 if self.is_cls_token else (self.patch**2)

        self.emb = nn.Linear(f, hidden) # (b, n, f)
        self.cls_token = nn.Parameter(torch.randn(1, 1, hidden)) if is_cls_token else None
        self.pos_emb = nn.Parameter(torch.randn(1,num_tokens, hidden))

        self.checkpointing = checkpointing

        E_proj_layerwise = None
        if param_sharing == "layerwise":
            E_proj_layerwise = get_EF((num_tokens, dim_k), method)

        if param_sharing == "layerwise" or param_sharing == "kv":
            one_kv_head = True
            share_kv = True
        elif param_sharing == "headwise":
            one_kv_head = True
            share_kv = False
        else:
            one_kv_head = False
            share_kv = False

        enc_list = [LinformerEncoder(num_tokens=num_tokens, dim_k=max(1, dim_k - i * k_reduce_by_layer), one_kv_head=one_kv_head, share_kv=share_kv, method=method, E_proj_layerwise=E_proj_layerwise, feats=hidden, mlp_hidden=mlp_hidden, head=head, dropout=dropout) for i in range(num_layers)]

        # self.enc = nn.Sequential(*enc_list)
        self.enc = nn.ModuleList(enc_list)
        self.fc = nn.Sequential(
            nn.LayerNorm(hidden),
            nn.Linear(hidden, num_classes) # for cls_token
        )


    def forward(self, x):
        out = self._to_words(x)
        out = self.emb(out)
        if self.is_cls_token:
            out = torch.cat([self.cls_token.repeat(out.size(0),1,1), out],dim=1)
        out = out + self.pos_emb

        # out = self.enc(out)

        if self.checkpointing:
            for blk in self.enc:
                out = checkpoint(blk, out)
        else:
            for blk in self.enc:
                out = blk(out)

        if self.is_cls_token:
            out = out[:,0]
        else:
            out = out.mean(1)
        out = self.fc(out)
        return out

    def _to_words(self, x):
        """
        (b, c, h, w) -> (b, n, f)
        """
        out = x.unfold(2, self.patch_size, self.patch_size).unfold(3, self.patch_size, self.patch_size).permute(0,2,3,4,5,1)
        out = out.reshape(x.size(0), self.patch**2 ,-1)
        return out
    