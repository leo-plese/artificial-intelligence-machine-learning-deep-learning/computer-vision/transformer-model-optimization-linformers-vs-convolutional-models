import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch
import time
import numpy as np

from autoaugment import CIFAR10Policy, SVHNPolicy
from criterions import LabelSmoothingCrossEntropyLoss
from da import RandomCropPaste


def get_criterion(args):
    if args.criterion=="ce":
        if args.label_smoothing:
            criterion = LabelSmoothingCrossEntropyLoss(args.num_classes, smoothing=args.smoothing)
        else:
            criterion = nn.CrossEntropyLoss()
    else:
        raise ValueError(f"{args.criterion}?")

    return criterion

def get_model(args):
    if args.model_name == 'vit':
        from vit import ViT
        net = ViT(
            args.in_c, 
            args.num_classes, 
            img_size=args.size, 
            patch=args.patch, 
            dropout=args.dropout, 
            mlp_hidden=args.mlp_hidden,
            num_layers=args.num_layers,
            hidden=args.hidden,
            head=args.head,
            is_cls_token=args.is_cls_token
            )
    elif args.model_name == "linformer":
        from linformer import Linformer
        net = Linformer(
            args.in_c,
            args.num_classes,
            img_size=args.size,
            patch=args.patch,
            dropout=args.dropout,
            mlp_hidden=args.mlp_hidden,
            num_layers=args.num_layers,
            hidden=args.hidden,
            head=args.head,
            is_cls_token=args.is_cls_token,
            dim_k=args.dim_k,
            param_sharing=args.param_sharing,
            method=args.method,
            k_reduce_by_layer=args.k_reduce_by_layer,
            checkpointing=args.checkpointing
        )
    else:
        raise NotImplementedError(f"{args.model_name} is not implemented yet...")

    return net

def get_transform(args):
    train_transform = []
    test_transform = []
    train_transform += [
        transforms.RandomCrop(size=args.size, padding=args.padding)
    ]
    if args.dataset != 'svhn':
        train_transform += [transforms.RandomHorizontalFlip()]
    
    if args.autoaugment:
        if args.dataset == 'c10' or args.dataset=='c100':
            train_transform.append(CIFAR10Policy())
        elif args.dataset == 'svhn':
            train_transform.append(SVHNPolicy())
        else:
            print(f"No AutoAugment for {args.dataset}")   

    train_transform += [
        transforms.ToTensor(),
        transforms.Normalize(mean=args.mean, std=args.std)
    ]
    if args.rcpaste:
        train_transform += [RandomCropPaste(size=args.size)]
    
    test_transform += [
        transforms.ToTensor(),
        transforms.Normalize(mean=args.mean, std=args.std)
    ]

    train_transform = transforms.Compose(train_transform)
    test_transform = transforms.Compose(test_transform)

    return train_transform, test_transform
    

def get_dataset(args):
    root = "data"
    if args.dataset == "c10":
        args.in_c = 3
        args.num_classes=10
        args.size = 32
        args.padding = 4
        args.mean, args.std = [0.4914, 0.4822, 0.4465], [0.2470, 0.2435, 0.2616]
        train_transform, test_transform = get_transform(args)
        train_ds = torchvision.datasets.CIFAR10(root, train=True, transform=train_transform, download=True)
        test_ds = torchvision.datasets.CIFAR10(root, train=False, transform=test_transform, download=True)

    elif args.dataset == "c100":
        args.in_c = 3
        args.num_classes=100
        args.size = 32
        args.padding = 4
        args.mean, args.std = [0.5071, 0.4867, 0.4408], [0.2675, 0.2565, 0.2761]
        train_transform, test_transform = get_transform(args)
        train_ds = torchvision.datasets.CIFAR100(root, train=True, transform=train_transform, download=True)
        test_ds = torchvision.datasets.CIFAR100(root, train=False, transform=test_transform, download=True)

    elif args.dataset == "svhn":
        args.in_c = 3
        args.num_classes=10
        args.size = 32
        args.padding = 4
        args.mean, args.std = [0.4377, 0.4438, 0.4728], [0.1980, 0.2010, 0.1970]
        train_transform, test_transform = get_transform(args)
        train_ds = torchvision.datasets.SVHN(root, split="train",transform=train_transform, download=True)
        test_ds = torchvision.datasets.SVHN(root, split="test", transform=test_transform, download=True)

    else:
        raise NotImplementedError(f"{args.dataset} is not implemented yet.")
    
    return train_ds, test_ds

def get_experiment_name(args):
    experiment_name = f"{args.model_name}_{args.dataset}"
    if args.autoaugment:
        experiment_name+="_aa"
    if args.label_smoothing:
        experiment_name+="_ls"
    if args.rcpaste:
        experiment_name+="_rc"
    if args.cutmix:
        experiment_name+="_cm"
    if args.mixup:
        experiment_name+="_mu"
    if args.off_cls_token:
        experiment_name+="_gap"
    print(f"Experiment:{experiment_name}")
    return experiment_name


def do_fwd_pass(net, bs, input_size, num_classes=10, train_mod=False, is_model_only=False):
    torch.cuda.empty_cache()

    print("BS =", bs, " , input_size =", input_size)
    x = torch.randn(bs, 3, input_size, input_size).cuda()
    label = torch.randint(num_classes, (bs,)).cuda()

    if train_mod:
        optimizer = torch.optim.Adam(net.model.parameters(), lr=net.hparams.lr, betas=(net.hparams.beta1, net.hparams.beta2), weight_decay=net.hparams.weight_decay)

        optimizer.zero_grad()

        if net.hparams.cutmix or net.hparams.mixup:
            if net.hparams.cutmix:
                x, label, rand_label, lambda_ = net.cutmix((x, label))
            elif net.hparams.mixup:
                if np.random.rand() <= 0.8:
                    x, label, rand_label, lambda_ = net.mixup((x, label))
                else:
                    x, label, rand_label, lambda_ = x, label, torch.zeros_like(label), 1.
            y = net.model(x)
            loss = net.criterion(y, label) * lambda_ + net.criterion(y, rand_label) * (1. - lambda_)
        else:
            y = net(x)
            loss = net.criterion(y, label)

        loss.backward()
        optimizer.step()
    else:
        with torch.no_grad():
            y = net(x)
            if not is_model_only:
                loss = net.criterion(y, label)

def get_max_batchsize(net, input_size, num_classes, train_mod = False):
    print("=== input_size", input_size)

    model = net.model
    model.to(torch.device("cuda"))

    cur_bs = 2
    while True:
        try:
            print("cur_bs =",cur_bs)
            do_fwd_pass(net, cur_bs, input_size, num_classes, train_mod)

            cur_bs *= 2
        except RuntimeError as err:
            print("!!! err =",err)
            top_bs = cur_bs
            bottom_bs = cur_bs//2
            break

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    while (top_bs - bottom_bs) > 1:
        mid_bs = (top_bs+bottom_bs)//2
        print("mid_bs =",mid_bs)
        try:
            do_fwd_pass(net, mid_bs, input_size, num_classes, train_mod)
            bottom_bs = mid_bs
        except RuntimeError:
            top_bs = mid_bs

    print("bottom =", bottom_bs)
    print("top =", top_bs)
    try:
        do_fwd_pass(net, top_bs, input_size, num_classes, train_mod)
        max_bs = top_bs
    except RuntimeError as err:
        print("!!! err =", err)
        max_bs = bottom_bs

    return max_bs

def get_max_inputsize(args):
    bs = args.batch_size
    print("=== bs", bs)

    cur_inpsize = args.size
    num_classes = args.num_classes

    while True:
        try:
            print("cur_is =",cur_inpsize)

            torch.cuda.empty_cache()
            args.size = cur_inpsize
            model = get_model(args)
            model.eval()
            model.to(torch.device("cuda"))

            do_fwd_pass(model, bs, cur_inpsize, num_classes, train_mod=False, is_model_only=True)

            cur_inpsize *= 2
        except RuntimeError as err:
            print("!!! err =",err)
            top_inpsize = cur_inpsize
            bottom_inpsize = cur_inpsize//2
            break

    print("bottom =", bottom_inpsize)
    print("top =", top_inpsize)
    while (top_inpsize - bottom_inpsize) > 1:
        mid_inpsize = (top_inpsize+bottom_inpsize)//2
        print("mid_is =",mid_inpsize)
        try:
            torch.cuda.empty_cache()
            args.size = mid_inpsize
            model = get_model(args)
            model.eval()
            model.to(torch.device("cuda"))

            do_fwd_pass(model, bs, mid_inpsize, num_classes, train_mod=False, is_model_only=True)

            bottom_inpsize = mid_inpsize
        except RuntimeError:
            top_inpsize = mid_inpsize

    print("bottom =", bottom_inpsize)
    print("top =", top_inpsize)
    try:
        torch.cuda.empty_cache()
        args.size = top_inpsize
        model = get_model(args)
        model.eval()
        model.to(torch.device("cuda"))

        do_fwd_pass(model, bs, top_inpsize, num_classes, train_mod=False, is_model_only=True)

        max_inpsize = top_inpsize
    except RuntimeError as err:
        print("!!! err =", err)
        max_inpsize = bottom_inpsize

    return max_inpsize

def measure_speed_wo_loading(model, bs, input_size, n=30):
    print("(batch_size, input_size) = ({}, {})".format(bs, input_size))

    model.eval()
    model.to(torch.device("cuda"))

    warm_up = 10
    torch.cuda.empty_cache()
    x = torch.randn(bs, 3, input_size, input_size).cuda()

    torch.cuda.synchronize()
    with torch.no_grad():
        for _ in range(warm_up):
            y = model(x)
            y = torch.softmax(y, 1)

    start = time.time()
    with torch.no_grad():
        for _ in range(n):
            y = model(x)
            y = torch.softmax(y, 1)
    torch.cuda.synchronize()

    end = time.time()
    return (bs * n) / (end - start)

def make_batchsize_speed_graph(model, max_bs, input_size, n=30, graph_freq = 250):
    print("*** input_size = {} ***".format(input_size))
    for bs_i in range(1, max_bs, graph_freq):
        print("bs =", bs_i)
        avg_fps = measure_speed_wo_loading(model, bs_i, input_size, n)
        print("AVG over ", n, " epochs (WITHOUT LOADING FROM MEMORY): img/sec (fps) =", avg_fps)
        print()


    print("bs = MAX_BS = ", max_bs)
    avg_fps = measure_speed_wo_loading(model, max_bs, input_size, n)
    print("AVG over ", n, " epochs (WITHOUT LOADING FROM MEMORY): img/sec (fps) =", avg_fps)

def make_inputsize_speed_graph(args, bs, max_inpsize, n=30, graph_freq = 50):
    print("*** batch_size = {} ***".format(bs))
    start_input_size = args.size
    for inpsize_i in range(start_input_size, max_inpsize, graph_freq):
        print("input_size =", inpsize_i)
        torch.cuda.empty_cache()
        args.size = inpsize_i
        model = get_model(args)
        avg_fps = measure_speed_wo_loading(model, bs, inpsize_i, n)
        print("AVG over ", n, " epochs (WITHOUT LOADING FROM MEMORY): img/sec (fps) =", avg_fps)
        print()


    print("input_size = MAX_INPUT_SIZE = ", max_inpsize)
    torch.cuda.empty_cache()
    args.size = max_inpsize
    model = get_model(args)
    avg_fps = measure_speed_wo_loading(model, bs, max_inpsize, n)
    print("AVG over ", n, " epochs (WITHOUT LOADING FROM MEMORY): img/sec (fps) =", avg_fps)



