import argparse

import torch
import torchvision
import pytorch_lightning as pl
import warmup_scheduler
import numpy as np

from utils import get_model, get_dataset, get_experiment_name, get_criterion, get_max_batchsize, measure_speed_wo_loading, make_batchsize_speed_graph, get_max_inputsize, make_inputsize_speed_graph
from da import CutMix, MixUp

parser = argparse.ArgumentParser()
parser.add_argument("--resume", default="")
parser.add_argument("--api-key", help="API Key for Comet.ml")
parser.add_argument("--dataset", default="c10", type=str, help="[c10, c100, svhn]")
parser.add_argument("--num-classes", default=10, type=int)
parser.add_argument("--model-name", default="vit", help="[vit, linformer]", type=str)
parser.add_argument("--patch", default=8, type=int)
parser.add_argument("--batch-size", default=128, type=int)
parser.add_argument("--eval-batch-size", default=1024, type=int)
parser.add_argument("--lr", default=1e-3, type=float)
parser.add_argument("--min-lr", default=1e-5, type=float)
parser.add_argument("--beta1", default=0.9, type=float)
parser.add_argument("--beta2", default=0.999, type=float)
parser.add_argument("--off-benchmark", action="store_true")
parser.add_argument("--max-epochs", default=200, type=int)
parser.add_argument("--dry-run", action="store_true")
parser.add_argument("--weight-decay", default=5e-5, type=float)
parser.add_argument("--warmup-epoch", default=5, type=int)
parser.add_argument("--precision", default=16, type=int)
parser.add_argument("--autoaugment", action="store_true")
parser.add_argument("--criterion", default="ce")
parser.add_argument("--label-smoothing", action="store_true")
parser.add_argument("--smoothing", default=0.1, type=float)
parser.add_argument("--rcpaste", action="store_true")
parser.add_argument("--cutmix", action="store_true")
parser.add_argument("--mixup", action="store_true")
parser.add_argument("--dropout", default=0.0, type=float)
parser.add_argument("--head", default=12, type=int)
parser.add_argument("--num-layers", default=7, type=int)
parser.add_argument("--hidden", default=384, type=int)
parser.add_argument("--mlp-hidden", default=384, type=int)
parser.add_argument("--off-cls-token", action="store_true")
parser.add_argument("--seed", default=42, type=int)
parser.add_argument("--project-name", default="VisionTransformer")

# arguments specific to Linformer
parser.add_argument("--dim-k", default=50, type=int, help="dim_k")
parser.add_argument("--param-sharing", default="layerwise", type=str, help="param_sharing linformer")
parser.add_argument("--method", default="learnable", type=str, help="method linformer")
parser.add_argument("--k-reduce-by-layer", default=0, type=int, help="k_reduce_by_layer linformer")
parser.add_argument("--checkpointing", default=0, type=int, help="checkpointing linformer")

parser.add_argument("--measure-max-bs-train", default=0, type=int, help="calc max batchsize on TRAIN")
parser.add_argument("--measure-max-bs", default=0, type=int, help="calc max batchsize on EVAL")
parser.add_argument("--measure-max-inpsize", default=0, type=int, help="calc max input size on EVAL")
parser.add_argument("--measure-speed-wo-loading", default=0, type=int, help="measure speeding without loading from memory")
parser.add_argument("--batchsize-speed-graph", default=0, type=int, help="measure speeding without loading from memory on BS RANGE [1, MAX BS EVAL]")
parser.add_argument("--inputsize-speed-graph", default=0, type=int, help="measure speeding without loading from memory on INPUT_SIZE RANGE [1, MAX INPUT SIZE EVAL]")
parser.add_argument("--max-input-size", default=0, type=int, help="provide MAX INPUT SIZE for making img resolution graph")

args = parser.parse_args()
torch.manual_seed(args.seed)
np.random.seed(args.seed)
args.benchmark = True if not args.off_benchmark else False
args.gpus = torch.cuda.device_count()
args.num_workers = 4*args.gpus if args.gpus else 8
args.is_cls_token = True if not args.off_cls_token else False
if not args.gpus:
    args.precision=32

if args.mlp_hidden != args.hidden*4:
    print(f"[INFO] In original paper, mlp_hidden(CURRENT:{args.mlp_hidden}) is set to: {args.hidden*4}(={args.hidden}*4)")

train_ds, test_ds = get_dataset(args)
train_dl = torch.utils.data.DataLoader(train_ds, batch_size=args.batch_size, shuffle=True, num_workers=args.num_workers, pin_memory=True)
test_dl = torch.utils.data.DataLoader(test_ds, batch_size=args.eval_batch_size, num_workers=args.num_workers, pin_memory=True)

class Net(pl.LightningModule):
    def __init__(self, hparams):
        super(Net, self).__init__()
        # self.hparams = hparams
        self.hparams.update(vars(hparams))
        self.model = get_model(hparams)
        self.criterion = get_criterion(args)
        if hparams.cutmix:
            self.cutmix = CutMix(hparams.size, beta=1.)
        if hparams.mixup:
            self.mixup = MixUp(alpha=1.)
        self.log_image_flag = hparams.api_key is None

    def forward(self, x):
        return self.model(x)

    def configure_optimizers(self):
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.hparams.lr, betas=(self.hparams.beta1, self.hparams.beta2), weight_decay=self.hparams.weight_decay)
        self.base_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(self.optimizer, T_max=self.hparams.max_epochs, eta_min=self.hparams.min_lr)
        self.scheduler = warmup_scheduler.GradualWarmupScheduler(self.optimizer, multiplier=1., total_epoch=self.hparams.warmup_epoch, after_scheduler=self.base_scheduler)
        return [self.optimizer], [self.scheduler]

    def training_step(self, batch, batch_idx):
        img, label = batch
        if self.hparams.cutmix or self.hparams.mixup:
            if self.hparams.cutmix:
                img, label, rand_label, lambda_= self.cutmix((img, label))
            elif self.hparams.mixup:
                if np.random.rand() <= 0.8:
                    img, label, rand_label, lambda_ = self.mixup((img, label))
                else:
                    img, label, rand_label, lambda_ = img, label, torch.zeros_like(label), 1.
            out = self.model(img)
            loss = self.criterion(out, label)*lambda_ + self.criterion(out, rand_label)*(1.-lambda_)
        else:
            out = self(img)
            loss = self.criterion(out, label)

        if not self.log_image_flag and not self.hparams.dry_run:
            self.log_image_flag = True
            self._log_image(img.clone().detach().cpu())

        acc = torch.eq(out.argmax(-1), label).float().mean()
        self.log("loss", loss)
        self.log("acc", acc)
        return loss

    def training_epoch_end(self, outputs):
        self.log("lr", self.optimizer.param_groups[0]["lr"], on_epoch=True)

    def validation_step(self, batch, batch_idx):
        img, label = batch
        out = self(img)
        loss = self.criterion(out, label)
        acc = torch.eq(out.argmax(-1), label).float().mean()
        self.log("val_loss", loss)
        self.log("val_acc", acc)
        return loss

    def _log_image(self, image):
        grid = torchvision.utils.make_grid(image, nrow=4)
        self.logger.experiment.log_image(grid.permute(1,2,0))
        print("[INFO] LOG IMAGE!!!")

def main(args):
    experiment_name = get_experiment_name(args)
    print(experiment_name)
    print("args.dry_run", args.dry_run)
    if args.api_key:
        print("[INFO] Log with Comet.ml!")
        logger = pl.loggers.CometLogger(
            api_key=args.api_key,
            save_dir="logs",
            project_name=args.project_name,
            experiment_name=experiment_name
        )
        refresh_rate = 0
    else:
        print("[INFO] Log with CSV")
        logger = pl.loggers.CSVLogger(
            save_dir="logs",
            name=experiment_name
        )
        refresh_rate = 1

    net = Net(args)

    if args.measure_max_bs == 1:
        # args.size = 32
        net.model.eval()
        max_bs_result = get_max_batchsize(net, args.size, args.num_classes)
        print("MAX BS EVAL =", max_bs_result)
        return
    elif args.measure_max_bs_train == 1:
        # args.size = 32
        net.model.train()
        max_bs_result = get_max_batchsize(net, args.size, args.num_classes, train_mod=True)
        print("MAX BS TRAIN =", max_bs_result)
        return
    elif args.measure_max_inpsize == 1:
        # args.batch_size = 128
        max_inpsize_result = get_max_inputsize(args)
        print("MAX INPUT_SIZE =", max_inpsize_result)
        return
    elif args.measure_speed_wo_loading == 1:
        n_epochs = 30
        batch_size, input_size = args.batch_size, (args.max_input_size if args.max_input_size != 0 else args.size)
        if args.max_input_size != 0:
            args.size = input_size
            net = Net(args)
        avg_fps = measure_speed_wo_loading(net.model, batch_size, input_size, n_epochs)
        print("AVG over ", n_epochs, " epochs (WITHOUT LOADING FROM MEMORY): img/sec (fps) =", avg_fps)
        return
    elif args.batchsize_speed_graph == 1:
        n_epochs = 30
        graph_freq = 250
        # args.batch_size = MAX_BS
        make_batchsize_speed_graph(net.model, args.batch_size, args.size, n_epochs, graph_freq)
        return
    elif args.inputsize_speed_graph == 1:
        n_epochs = 30
        graph_freq = 50
        # args.max_input_size = MAX_INPUT_SIZE
        make_inputsize_speed_graph(args, args.batch_size, args.max_input_size, n_epochs, graph_freq)
        return


    trainer = pl.Trainer(precision=args.precision, fast_dev_run=args.dry_run, gpus=args.gpus, benchmark=args.benchmark,
                         logger=logger, max_epochs=args.max_epochs, weights_summary="full",
                         progress_bar_refresh_rate=refresh_rate)
    trainer.fit(model=net, train_dataloaders=train_dl, val_dataloaders=test_dl,
                ckpt_path=(args.resume if args.resume else None))
    if not args.dry_run:
        model_path = f"weights/{experiment_name}.pth"
        torch.save(net.state_dict(), model_path)
        if args.api_key:
            logger.experiment.log_asset(file_name=experiment_name, file_data=model_path)


if __name__ == "__main__":
    main(args)