# Transformer Model Optimization Linformers vs Convolutional Models

My research project in Computer Vision. Implemented in Python using PyTorch, timm and Matplotlib library.

Focus of project is adaptation of Vision Transformer ViT (https://arxiv.org/pdf/2010.11929) as well as improvements proposed by Data-efficient Image Transformer DeiT (https://arxiv.org/abs/2012.12877), by implementing a novel self-attention mechanism as proposed by Linformer transformer (https://arxiv.org/abs/2006.04768) but in domain of Computer Vision instead of Natural Language Processing.

These transformer models are briefly compared to ResNet-18 (residual networks ResNet https://arxiv.org/abs/1512.03385) convolutional models as a starting point, also in variant with attention mechanism added in form of AtResNet-18 ("attention ResNet").

Implementation is in PyTorch deep learning framework.

Project was conducted in 2 phases - 2 corresponding folders:

1 resnet_atresnet_vit_deit: initial comparison of ResNet-18, AtResNet-18, ViT, Linformer

2 vit_linformer: comparison of ViT and Linformer models with different linear projection dimensions (k) (models trained from scratch). 
Implementation is based on efficient implementation of ViT (https://github.com/omihub777/ViT-CIFAR). Generalization ability of trained Linformer models is compared and successfully confirmed by comparing its classification accuracy to that of models trained by official implementation https://github.com/lucidrains/linformer 

Project duration: Nov 2020 - Mar 2021.
